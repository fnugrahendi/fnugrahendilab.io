import React from 'react'
import styled from 'styled-components'
import Typed from 'react-typed'
// import { Link } from 'gatsby'
import Layout from '../layout'

const IndexPageStyle = styled.div`
  display: block;
  max-width: 100%;
  padding: 2.5vw;
  color: white;
`

const MainTextLeft = styled.h2`
  display: inline-block;
  width: 65%;
  text-align: left;
  font-size: 6.25rem;

  @media (max-width: 976px) {
    width: 100%;
  }

  @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
    font-size: 4.15rem;
  }
`

const MainTextRight = styled.div`
  display: inline-block;
  width: 35%;
  text-align: right;
  p {
    margin-bottom: 0;
    font-family: sans-serif;
  }

  @media (max-width: 976px) {
    width: 100%;
    text-align: left;
    margin-top: 2rem;
    min-height: 4rem;
  }
`

const InfoText = styled.div`
  margin: 10px 0;
  font-weight: 300;
  a {
    color: white;
    margin: 0 5px;
    text-decoration: none;
    font-family: sans-serif;
  }
  a:not(:last-child) {
    border-right: 1px solid white;
    padding-right: 5px;
  }
`

const IndexPage = () => (
  <Layout>
    <IndexPageStyle>
      <MainTextLeft>Ferindra Nugrahendi</MainTextLeft>
      <MainTextRight>
        <Typed
          strings={[
            'Software Engineer: Frontend',
            'Software Engineer: Frontend - ReactJS',
            'Software Engineer: Frontend - ReactJS+Redux',
            'Software Engineer: Mobile - ReactNative',
            'Software Engineer: Backend - ASP .NET ',
            'Software Engineer: Backend - Node.Js',
            'Software Engineer: Backend - Python',
          ]}
          typeSpeed={30}
          backSpeed={50}
          loop
          style={{ fontFamily: 'sans-serif' }}
        />
      </MainTextRight>
      <InfoText>
        <a href="https://github.com/fnugrahendi" target="_blank">github</a>
        <a href="https://gitlab.com/fnugrahendi" target="_blank">gitlab</a>
        <a href="https://linkedin.com/in/fnugrahendi" target="_blank">linkedin</a>
        <a href="https://fnugrahendi.wordpress.com" target="_blank">blog</a>
      </InfoText  >
      {/* <Link to="/page-2/">Go to page 2</Link> */}
    </IndexPageStyle>
  </Layout>
)

export default IndexPage
