module.exports = {
  pathPrefix: `/gatsby`,
  siteMetadata: {
    title: `Ferindra N`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`
  ],
}
